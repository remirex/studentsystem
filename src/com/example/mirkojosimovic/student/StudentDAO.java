package com.example.mirkojosimovic.student;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class StudentDAO {

    private String jdbcURL;
    private String jdbcUsername;
    private String jdbcPassword;
    private Connection jdbcConnection;

    public StudentDAO(String jdbcURL, String jdbcUsername, String jdbcPassword) {
        this.jdbcURL = jdbcURL;
        this.jdbcUsername = jdbcUsername;
        this.jdbcPassword = jdbcPassword;
    }

    protected void connect() throws SQLException {
        if (jdbcConnection == null || jdbcConnection.isClosed()) {
            try {
                Class.forName("com.mysql.jdbc.Driver");
            } catch (ClassNotFoundException e) {
                throw new SQLException(e);
            }

            jdbcConnection = DriverManager.getConnection(jdbcURL, jdbcUsername, jdbcPassword);
        }
    }

    protected void disconnect() throws SQLException {
        if (jdbcConnection != null && !jdbcConnection.isClosed()) {
            jdbcConnection.close();
        }
    }

    public List<Student> listAllStudents() throws SQLException {
        List<Student> listStudent = new ArrayList<>();

        String sql = "SELECT * FROM web_student";

        connect();

        Statement statement = jdbcConnection.createStatement();
        ResultSet resultSet = statement.executeQuery(sql);

        while (resultSet.next()) {
            int id = resultSet.getInt("id");
            String firstName = resultSet.getString("first_name");
            String lastName = resultSet.getString("last_name");
            String email = resultSet.getString("email");
            String department = resultSet.getString("department");
            int studentNum = resultSet.getInt("student_number");

            Student student = new Student(id, firstName, lastName, email, department, studentNum);
            listStudent.add(student);
        }

        resultSet.close();
        statement.close();

        disconnect();

        return listStudent;
    }

    public boolean insertStudent(Student student) throws SQLException {
        String sql = "INSERT INTO web_student (first_name, last_name, email, department, student_number) VALUE (?, ?, ?, ?, ?)";

        connect();

        PreparedStatement statement = jdbcConnection.prepareStatement(sql);
        statement.setString(1, student.getFirstName());
        statement.setString(2, student.getLastName());
        statement.setString(3, student.getEmail());
        statement.setString(4, student.getDepartment());
        statement.setInt(5, student.getStudentNum());

        boolean rowInserted = statement.executeUpdate() > 0; // executeUpdate() method return 1 to indicate one record was inserted

        statement.close();
        disconnect();

        return rowInserted;
    }

    public boolean updateStudent(Student student) throws SQLException {
        String sql = "UPDATE web_student SET first_name = ?, last_name = ?, email = ?, department = ?, student_number = ? WHERE id = ?";

        connect();

        PreparedStatement statement = jdbcConnection.prepareStatement(sql);
        statement.setString(1, student.getFirstName());
        statement.setString(2, student.getLastName());
        statement.setString(3, student.getEmail());
        statement.setString(4, student.getDepartment());
        statement.setInt(5, student.getStudentNum());
        statement.setInt(6, student.getId());

        boolean rowUpdated = statement.executeUpdate() > 0;

        statement.close();
        disconnect();

        return rowUpdated;
    }

    public Student getStudent(int id) throws SQLException {
        Student student = null;

        String sql = "SELECT * FROM web_student WHERE id = ?";

        connect();

        PreparedStatement statement = jdbcConnection.prepareStatement(sql);
        statement.setInt(1, id);

        ResultSet resultSet = statement.executeQuery();

        if (resultSet.next()) {
            String firstName = resultSet.getString("first_name");
            String lastName = resultSet.getString("last_name");
            String email = resultSet.getString("email");
            String department = resultSet.getString("department");
            int studentNum = resultSet.getInt("student_number");

            student = new Student(id, firstName, lastName, email, department, studentNum);
        }

        resultSet.close();
        statement.close();

        return student;
    }

    public boolean deleteStudent(Student student) throws SQLException {
        String sql = "DELETE FROM web_student WHERE id = ?";

        connect();

        PreparedStatement statement = jdbcConnection.prepareStatement(sql);
        statement.setInt(1, student.getId());

        boolean rowDeleted = statement.executeUpdate() > 0;

        statement.close();
        disconnect();

        return rowDeleted;
    }
}
