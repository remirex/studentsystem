<%--
  Created by IntelliJ IDEA.
  User: mirko
  Date: 1.4.20.
  Time: 20:28
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <title>Title</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
</head>
<body>
<nav class="navbar navbar-expand-lg navbar-dark bg-dark">
    <a class="navbar-brand" href="#">MJ</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarNav">
        <ul class="navbar-nav">
            <li class="nav-item active">
                <a class="nav-link" href="#">List All Student <span class="sr-only">(current)</span></a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="#">Add New Student</a>
            </li>
        </ul>
    </div>
</nav>

<div class="container">
    <h3 class="mt-5">Edit Student</h3>
    <form class="mt-5" method="post" action="update">
        <div class="form-row">
            <input type="hidden" name="id" value="<c:out value="${student.id}"/>">
            <div class="form-group col-md-6">
                <input type="text" class="form-control" placeholder="First name" name="firstName" value="<c:out value="${student.firstName}"/>">
            </div>
            <div class="form-group col-md-6">
                <input type="text" class="form-control" placeholder="Last name" name="lastName" value="<c:out value="${student.lastName}"/>">
            </div>
            <div class="form-group col-md-6">
                <input type="email" class="form-control" placeholder="Email" name="email" value="<c:out value="${student.email}"/>">
            </div>
            <div class="form-group col-md-6">
                <input type="number" class="form-control" placeholder="Student Number" name="studentNum" value="<c:out value="${student.studentNum}"/>">
            </div>
            <div class="form-group col-md-6">
                <input type="text" class="form-control" placeholder="Department" name="department" value="<c:out value="${student.department}"/>">
            </div>
        </div>
        <div class="form-group">
            <input type="submit" class="btn btn-warning" value="Update Student">
        </div>
    </form>
</div>

</body>
</html>